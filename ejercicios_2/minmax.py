'''
Crear una lista la cual almacene 10 números
positivos ingresados por el usuario,
mostrar en pantalla: la suma de todos los
números, el promedio, el número mayor y el número menor.
'''
list = []
suma = 0
promedio = 0
mayor = 0
menor = 0
for i in range(0, 10):
    num = int(input("Escribe un numero positivo:\n"))
    if num > 0:
        suma += num
        list.append(num)

promedio = suma / len(list)
mayor = max(list)
menor = min(list)
print("La suma es {s} el promedio es {p} el mayor es {ma} el menor es {mi}".format(s = suma,\
                                                                                   p = promedio,\
                                                                                   ma = mayor,\
                                                                                   mi = menor))