palabra = str(input("Escribe una lista de palabras, separadas por coma(,):\n")) 
print("===========================")
# de string a lista usando split
lista = palabra.split(', ')
pal = ""
for p in lista:
    pal = p[::-1] # ordemamos la palabra de fin a inicio
    if p == pal:
        print("{} es palindromo".format(p))
