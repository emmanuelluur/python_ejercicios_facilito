'''
Dado un diccionario, el cual almacena las calificaciones de un
alumno, siendo las llaves los nombres de las materia y los valores
las calificación, mostrar en pantalla el promedio del alumno.
'''

calificaciones = {'calculo':10, 'dibujo':5, 'programacion': 8, 'historia': 9}
solo_cal = calificaciones.values() # extraemos solo los valores del diccionario
cal_max = max(list(solo_cal)) # convierte a lista para usar la funcion max
promedio =  mi0
TOTAL_MATERIAS = len(calificaciones)
suma_calificaciones = 0
for cal in solo_cal:
    suma_calificaciones += cal
promedio = suma_calificaciones / TOTAL_MATERIAS
print("El promedio del estudiante es: {p}".format(p = promedio))
print("La calificacion mas alta es: {m}".format(m = cal_max))
