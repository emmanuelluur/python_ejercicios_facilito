from math import sin,radians,sqrt, pow
"""
Calcular y mostrar en pantalla la
longitud de la sombra de un edificio
de 20 metros de altura cuando el ángulo
que forman los rayos del sol con el suelo es de 22º
"""
co = 20
ango= 22
anga = 68

h = 22 / sin(radians(anga))

ca = sqrt(pow(h,2) - pow(co,2))

print("La longitud de sombra de un de edificio de 20m de altura es {s:.2F} m".format(s=ca))