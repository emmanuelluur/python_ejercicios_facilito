"""
Calcular la cantidad de segundos
que le toma a la luz viajar del
sol a Marte y mostrar el resultado en pantalla
"""

VELOCIAD_LUZ =  299792458 # m/s
DISTANCIA_SOL_MARTE = 227940000 #km
DISTANCIA_METROS = DISTANCIA_SOL_MARTE * 1000

tiempo = 0
#formula t = d / v
tiempo = DISTANCIA_METROS / VELOCIAD_LUZ

print("La luz tarda en llegar a marte {s:.2F} segs".format(s = tiempo))

