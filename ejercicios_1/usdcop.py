"""
Convertir la cantidad de dólares
ingresados por un usuario a pesos
colombianos y mostrar el resultado en pantalla
"""
usd = 3212.18 # cop
usd_user = 0
cop = 0

usd_user = float(input("Ingrese dolares a convertir: \n$ "))

cop = usd * usd_user

print("El total es ${cop:.2F} COP".format(cop = cop))