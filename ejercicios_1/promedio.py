"""
Mostrar en pantalla el promedio
de un alumno que ha cursado 5
materias (Español, Matemáticas,
Economía, Programación, Ingles)
"""
materias = {
    'esp': 90,
    'mat': 80,
    'eco': 90,
    'pro': 70,
    'ing': 80,
}

c = 0
for materia in materias:
    c+=materias[materia]
cal = c / len(materias)
print("El promedio del usuario es {p}".format(p=cal))