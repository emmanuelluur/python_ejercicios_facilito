"""
Mostrar en pantalla la
cantidad de meses transcurridos
desde la fecha de nacimiento de un usuario
"""
from datetime import datetime
#fecha = datetime(1990,11,22)
a = int(input("Ingrese año nacimiento:\n"))
m = int(input("Ingrese mes nacimiento:\n"))
d = int(input("Ingrese día nacimiento:\n"))
fecha = datetime(a,m,d)

diferencia = datetime.now() - fecha
print("De {a} a {h} han pasado {d} dias".format(a=a,d=diferencia.days, h=datetime.now()))