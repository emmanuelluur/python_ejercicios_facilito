from math import pi
"""
Calcular el número de vueltas que da una
llanta en 1 km, dado que el diámetro de la
llanta es de 50 cm, mostrar el resultado
en pantalla
"""

diametro = 50  # cm
radio = diametro / 2
distancia = 1 #km
# formula rodada = 2 * pi * R
rodada = (2 * (pi * radio)) / 100 # pasamos de cm a m

vueltas = ((distancia * 1000) - 1) / rodada # pasamos distancia de km a m
print("La llanta da {v:.0f} vueltas en {d} km".format(v=vueltas, d=distancia))
