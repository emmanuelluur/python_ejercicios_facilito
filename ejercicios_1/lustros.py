"""
Mostrar en pantalla la cantidad
de segundos que tiene un lustro
"""
CONST_SEGS = 3.154e+7

segs_lustro = CONST_SEGS * 5

print("En 1 lustro hay {seg} segundos".format(seg=segs_lustro))