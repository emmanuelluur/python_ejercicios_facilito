"""
Convertir los grados centígrados
ingresados por un usuario a grados
Fahrenheit y mostrar el resultado en pantalla
Formula (°C × 9/5) + 32 = °F 
"""
f = 0
c = 0

c = float(input("Ingrese grados Celsius:\n"))

f = (c * (9/5)) + 32

print("{c:.2F} °C son {f:.2F} °F".format(c = c, f = f))